# Job and Operation Entropy in Job Shop Scheduling: A Dataset

The following repository contains the code used to generate the data described in the data publication *Job and Operation Entropy in Job Shop Scheduling:
A Dataset* submitted to the journal *ing.grid*.

## How to use

To generate your own dataset, first create a seperate conda environment. Go to the main repository directory, and use the command:

```
conda env create -f entropy_dataset_env.yml
```

Secondly, create a config file. Examples are visible in the config folder including descriptions of each parameter. Then run the generation script using the command:

```
python dataset_generator.py --config_path <path/to/config/file>
```

After the dataset is generated you can use dataset_analyzer to check if it has correct entropy properties. To do so, use the following command:

```
python source/util/dataset_analyzer.py --dataset_path <path/to/dataset>
```

## Notes

Generation of the dataset itself does not take much time, however calculating the optimal time using the ortools library can be extremely slow. If it is not required for your project, it is possible to skip the calculation of optima by specifying the proper parameter in the config file.


