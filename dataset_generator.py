import sys
import os

sys.path.append(os.getcwd())

from source.generators.jsp_instance_entropy_generators import RandomJSPGenerator, RandomJSPGeneratorOperationDistirbution
from source.generators.jsp_dataset_entropy_generators import RandomJSPDatasetGeneratorWithJobPoolEntropy, RandomJSPDatasetGeneratorWithOperationPoolEntropy
from source.util.entropy_functions import EntropyOptimizer, calculate_entropy_from_operations_list
from scipy.stats import entropy
import argparse

import yaml

def generate_dataset(config_file):
    num_jobs = config_file['num_jobs']
    num_operations = config_file['num_operations']
    max_op_duration = config_file['max_op_duration']
    
    if 'optimizer_config' in config_file:
        optimizer_config = config_file['optimizer_config']
    else:
        optimizer_config = None
    
    if 'entropy_ratio' in config_file:
        entropy_ratio = config_file['entropy_ratio']
    else:
        entropy_ratio = None

    if optimizer_config!=None:
        optimizer = EntropyOptimizer(
            output_size=int(optimizer_config['output_size']), 
            hidden_size=int(optimizer_config['hidden_size']), 
            learning_rate=float(optimizer_config['learning_rate']), 
            num_epochs=int(optimizer_config['num_epochs']), 
            max_episodes=int(optimizer_config['max_episodes']), 
            precision=float(optimizer_config['precision']),
            verbose=True
            )
        entropies = optimizer.find_entropies(entropy_ratio)
        
        max_entropy = calculate_entropy_from_operations_list(range(optimizer_config['output_size']))


        for ratio in entropies:
            output_entropy = entropy(entropies[ratio])
            relative_entropy = output_entropy/max_entropy
            entropy_diff = abs(relative_entropy-ratio)
            assert entropy_diff < 0.05, f"One of the entropies does not meet the required ratio ({ratio}). Rerun the code again."


    dir_path = config_file['target_path']

    if config_file['generator_type'] == 'random':
        instance = RandomJSPGenerator(num_jobs, num_operations, max_op_duration, dataset_size=config_file['dataset_size'])
        instance.generate()
        instance.save_dataset(dir_path)
    elif config_file['generator_type'] == 'intra-instance-operation':
        for i in entropies:
            instance = RandomJSPGeneratorOperationDistirbution(num_jobs, num_operations, max_op_duration, entropy_distribution=entropies[i], dataset_size=config_file['dataset_size'])
            instance.generate()
            instance.save_dataset(f"{dir_path}/{config_file['name-id']}-{str(i).replace('.', '')}", solve_optimal=config_file['solve_optimal'])
    elif config_file['generator_type'] == 'inter-instance-job':
        for i in entropies:
            instance_dataset_job = RandomJSPDatasetGeneratorWithJobPoolEntropy(num_jobs=num_jobs, num_operations=num_operations, max_op_duration=max_op_duration, entropy_distribution=entropies[i], dataset_size=config_file['dataset_size'])
            instance_dataset_job.generate()
            instance_dataset_job.save_dataset(f"{dir_path}/{config_file['name-id']}-{str(i).replace('.', '')}", solve_optimal=config_file['solve_optimal'])
    elif config_file['generator_type'] == 'inter-instance-operation':
        for i in entropies:
            instance_dataset_job = RandomJSPDatasetGeneratorWithOperationPoolEntropy(num_jobs=num_jobs, num_operations=num_operations, max_op_duration=max_op_duration, entropy_distribution=entropies[i], dataset_size=config_file['dataset_size'])
            instance_dataset_job.generate()
            instance_dataset_job.save_dataset(f"{dir_path}/{config_file['name-id']}-{str(i).replace('.', '')}", solve_optimal=config_file['solve_optimal'])

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description="Script for generating jsp datasets.")
    parser.add_argument("--config_path", help="Path to the config file with generation parameters.", default="")
        
    args = parser.parse_args()

    with open(args.config_path, "r") as stream:
        config_file=yaml.safe_load(stream)
    print(config_file)

    generate_dataset(config_file)
    
       