from typing import Dict
from pathlib import Path
import json

def get_job_matrices(jsp_instance: Dict):
    machine_types = []
    durations = []
    for job in jsp_instance['jobs']:
        machine_types.append([operation.machine_type for operation in job])
        durations.append([operation.duration for operation in job])
    
    return machine_types, durations

class JSPWriterJSON:
    def write_instance(self, instance: Dict, path: Path, id: str):
        machine_types, durations = get_job_matrices(instance)
        
        json_file = {
            'id': id,
            'opt_time': instance['opt_time'],
            'intra_instance_operation_entropy': instance['intra-instance-operation-entropy'],
            'num_ops_per_job': instance['num_ops_per_job'],
            'max_op_time': instance['max_op_time'],
            'machine_types': machine_types, 
            'durations': durations
            }

        with open(path, 'w') as file:
            json.dump(json_file, file, indent=4)
