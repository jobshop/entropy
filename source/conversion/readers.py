from source.generators.jsp_generator import Operation
import json
from typing import Dict, List
from source.util.entropy_functions import calculate_entropy_from_operations_list, collect_all_operations

class JSPReaderJSON:
    def read_instance(self, path: str) -> Dict:

        with open(path, 'r') as file:
            input_file = json.load(file)

        jobs = []

        for (job_machine_types, job_durations) in zip(input_file['machine_types'], input_file['durations']):

            operations = []
            for (operation_machine_type, operation_duration) in zip(job_machine_types, job_durations):
                operations.append(Operation(operation_machine_type, operation_duration))

            jobs.append(operations)
        
        all_operations = collect_all_operations(jobs)

        return {"jobs": jobs, 
                "num_ops_per_job": input_file['num_ops_per_job'], 
                "max_op_time": input_file['max_op_time'], 
                "opt_time": input_file['opt_time'],
                "num_jobs": len(jobs),
                "intra-instance-operation-entropy": calculate_entropy_from_operations_list(all_operations, base=len(all_operations))}



class JSPReaderGoogleOR:
    def read_instance(self, path: str) -> List:       

        with open(path, 'r') as file:
            input_file = json.load(file)

        jobs = []

        for job_machine_types, job_durations in zip(input_file['machine_types'], input_file['durations']):
            
            operations = []
            for operation_machine_type, operation_duration in zip(job_machine_types, job_durations):
                operations.append([operation_machine_type, operation_duration])
            
            jobs.append(operations)


        return jobs
    