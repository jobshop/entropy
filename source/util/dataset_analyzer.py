import os
import sys
import json
from pathlib import Path
import argparse
sys.path.append(os.getcwd())
from source.util.entropy_functions import calculate_entropy_from_operations_list
from source.conversion.readers import JSPReaderJSON

def replace_with_indexes(data):
    """Replace each sublist in the given list with an index."""
    index_map = {}
    index_counter = 0
    result = []
    for sublist in data:
        sublist_tuple = tuple(sublist)
        if sublist_tuple not in index_map:
            index_map[sublist_tuple] = index_counter
            index_counter += 1
        result.append(index_map[sublist_tuple])
    return result

def collect_minimal_job_info(jobs):
    """Collect a list of all operations in the given jobs."""
    all_operations = []
    for job in jobs:
        job_operations = [(i.machine_type, i.duration) for i in job]
        all_operations.append(job_operations)
    return all_operations

class DatasetAnalyzer():
    """Class for analyzing a dataset of instances. Calculating entropies and other statistics."""
    def __init__(self, dataset_path: str, instance_reader) -> None:
        self.dataset_path = Path(dataset_path)
        self.instance_reader = instance_reader

    def analyze_dataset(self):

        inter_instance_job_entropies = {}
        inter_instance_op_entropies = {}
        results = []
        distribution_list = {}


        for entropy in os.listdir(self.dataset_path):
            entropy_data_path = self.dataset_path.joinpath(entropy)

            entropy_job_list = []
            entropy_operations_list = []

            for file_name in os.listdir(entropy_data_path):
                instance = self.instance_reader.read_instance(entropy_data_path.joinpath(file_name))

                entropy_job_list += collect_minimal_job_info(instance['jobs'])
                entropy_operations_list += collect_minimal_job_info(instance['jobs'])
                
                file_data = {
                    'entropy': entropy, 
                    'file_name': file_name,
                    'intra-instance-operation-entropy': instance['intra-instance-operation-entropy'], 
                    'num_jobs': instance['num_jobs'], 
                    'num_ops_per_job': instance['num_ops_per_job'], 
                    'max_op_time': instance['max_op_time'], 
                    'opt_time': instance['opt_time'],
                    }
                
                results.append(file_data)
            
            entropy_operations_list = [job for instance in entropy_operations_list for job in instance]
           
            indexed_jobs = replace_with_indexes(entropy_job_list)

            inter_instance_job_entropies[entropy] = calculate_entropy_from_operations_list(indexed_jobs, base=len(indexed_jobs))

            inter_instance_op_entropies[entropy], distribution_list[entropy] = calculate_entropy_from_operations_list(entropy_operations_list, base=len(entropy_operations_list), include_probabilities=True)
        
        self.inter_instance_job_entropies = inter_instance_job_entropies
        self.inter_instance_op_entropies = inter_instance_op_entropies
        self.mean_num_ops_per_job = sum(r['num_ops_per_job'] for r in results) / len(results)
        self.mean_num_jobs = sum(r['num_jobs'] for r in results) / len(results)
        self.mean_max_op_time = sum(r['max_op_time'] for r in results) / len(results)
        self.distribution_lists = distribution_list
        self.mean_intra_instance_op_entropies = {}

        for r in results:
            if r['entropy'] in self.mean_intra_instance_op_entropies:
                self.mean_intra_instance_op_entropies[r['entropy']] += r['intra-instance-operation-entropy']
            else:
                self.mean_intra_instance_op_entropies[r['entropy']] = r['intra-instance-operation-entropy']
        
        num_entropies = len(self.mean_intra_instance_op_entropies)
        for k, v in self.mean_intra_instance_op_entropies.items():
            self.mean_intra_instance_op_entropies[k] = v / (len(results)/num_entropies)

        self.all_results = {"mean_num_ops_per_job": self.mean_num_ops_per_job,
                            "mean_num_jobs": self.mean_num_jobs,
                            "mean_max_op_time": self.mean_max_op_time,
                            "intra-instance-operation-entropy": self.mean_intra_instance_op_entropies,
                            "inter-instance-job-entropy": self.inter_instance_job_entropies,
                            "inter-instance-operation-entropy": self.inter_instance_op_entropies,}
        

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description="Script for analyzing jsp datasets.")
    parser.add_argument("--dataset_path", help="Path to the dataset file.", default="")
        
    args = parser.parse_args()

    analyzer = DatasetAnalyzer(dataset_path=Path(args.dataset_path), instance_reader=JSPReaderJSON())
    analyzer.analyze_dataset()
    print(json.dumps(analyzer.all_results, indent=4))
