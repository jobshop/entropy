from collections import Counter
from typing import List
import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
from scipy.stats import entropy
import plotly.graph_objects as go
from plotly.subplots import make_subplots


def calculate_entropy_from_operations_list(operation_types: List, base=None, include_probabilities=False) -> float:
    """Calculates the entropy of a list of operations."""
    counts = Counter(operation_types)
    probabilities = [count/len(operation_types) for count in counts.values()]
    entropy_val = entropy(probabilities, base=base)
    if include_probabilities:
        return entropy_val, probabilities
    else:
        return entropy_val

def collect_all_operations(jobs: List) -> List[tuple]:
    """Collect a list of all operations in the given jobs."""
    all_operations = []
    for job in jobs:
        job_operations = [(i.machine_type, i.duration) for i in job]
        all_operations += job_operations
    return all_operations

class ProbabilityAdjustmentNetwork(nn.Module):
    """A simple neural network that takes in a vector of probabilities and adjusts them to have a certain entropy."""
    def __init__(self, input_size, hidden_size, output_size):
        super().__init__()
        self.fc1 = nn.Linear(input_size, hidden_size)
        self.fc2 = nn.Linear(hidden_size, output_size)

    def forward(self, x):
        x = torch.sigmoid(self.fc1(x))
        x = self.fc2(x)
        return F.softmax(x, dim=0)


def get_entropy_loss(pred, target):
    """Calculates the loss between the entropy of the predicted probabilities and the target entropy."""
    regularization_term = torch.max(pred)-torch.mean(pred)
    distribution = torch.distributions.Categorical(probs=pred)
    entropy = distribution.entropy()    
    loss = 0.5*(entropy-target)**2 + 0.5*(regularization_term)**2
    return loss

def check_validity(entropy_list, threshold=0.1):
    """Checks if the distribution is valid, makes sure that it is uniformly distributed."""
    if len(entropy_list) == 0:
        return False
    entropy_mean = np.mean(entropy_list)
    if len([i for i in entropy_list if i > entropy_mean]) > threshold*len(entropy_list):
        return True

class EntropyOptimizer:
    """A class that optimizes the probabilities of a list of operations to have a certain entropy."""
    def __init__(self, output_size: int, hidden_size: int=20, learning_rate: float=0.1, num_epochs: int=200, max_episodes: int=200, precision: float=0.1, verbose=False):
        self.input_size = output_size
        self.hidden_size = hidden_size
        self.output_size = output_size
        self.learning_rate = learning_rate
        self.num_epochs = num_epochs
        self.max_episodes = max_episodes
        self.precision = precision
        self.verbose = verbose
        
    def _find_probabilities(self, target):
        """Finds the probabilities that will result in the target entropy."""
        network = ProbabilityAdjustmentNetwork(input_size=self.input_size, hidden_size=self.hidden_size, output_size=self.output_size)
        optimizer = torch.optim.AdamW(network.parameters(), lr=self.learning_rate)
        input_data = torch.ones(self.input_size)

        
        # Train the network
        for epoch in range(self.num_epochs):
            # Forward pass
            output = network(input_data)
            
            loss = get_entropy_loss(output, target)
                            
            if loss <= 0.0005:
                break

            # Backward and optimize
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()

        with torch.no_grad():
            output = network(input_data).detach()

        return output
    
    def _entropy_distribution_finder(self, entropy_ratio):
        """
        Finds the probabilities that will result in the target entropy.
        It will initialize new network for the entropy search until the target entropy is reached.
        """
        max_entropy = calculate_entropy_from_operations_list(list(range(self.output_size)))
        target_entropy = max_entropy*entropy_ratio
        i = 0
        abs_entropy = target_entropy
        best_output = []
        current_best = target_entropy

        failure= False
        
        while True:

            if abs_entropy <= self.precision and check_validity(best_output):
                break

            if i > self.max_episodes:
                if self.verbose:
                    print(f"failure with size={self.output_size}, and entropy_ratio={entropy_ratio}, best_output={entropy(best_output)-target_entropy}")
                failure = True
                break
            
            output = self._find_probabilities(target=target_entropy)

            # Remove the ones that are smaller then the output size and add the rest to the last one, just to make sure they sum up to 1.
            filtered_output = [float(i) for i in output if i > 1/self.output_size]

            if sum(filtered_output) != 1:                        
                missing_fraction = (1-sum(filtered_output))/len(filtered_output)
                filtered_output = [i + missing_fraction for i in filtered_output]
            
            abs_entropy = abs(entropy(filtered_output)-target_entropy)
            if abs_entropy <= current_best:
                current_best=abs_entropy
                best_output=filtered_output
            i += 1
        if self.verbose: 
            print(f"found entropy = {entropy(best_output)} vs target={target_entropy} at epoch={i}. found/max = {entropy(best_output)/max_entropy}. Output={best_output}, sum={round(sum(best_output),4)}")
        return best_output, failure
    

    
    def find_entropies(self, entropy_ratio=None):
        entropies_dict = {}

        if entropy_ratio is not None:
            outputs, failure = self._entropy_distribution_finder(entropy_ratio=entropy_ratio)
            entropies_dict[entropy_ratio] = outputs
        else:
            for i in range(2, 9):
                ratio = round(i*0.1, 2)
                outputs, failure = self._entropy_distribution_finder(entropy_ratio=ratio)
                entropies_dict[ratio] = outputs
        return entropies_dict
    

def visualize_entropies(entropies, max_entropy, output_size):
    fig = make_subplots(rows=3, cols=3, shared_xaxes=True, subplot_titles=[f"Entropy Ratio = {i} vs {round(entropy(entropies[i])/max_entropy,2)} " for i in entropies.keys()])
    item = 0
    row = 1
    for i, values in entropies.items():
        entropy_mean = np.mean(values)
        entropy_std = np.std(values)
        bar_colors = ['#FF6692' if val > entropy_mean else '#00CC96' for val in values]

        fig.add_trace(go.Bar(x=np.arange(len(values)), y=values, name=f"Entropy Ratio = {i}", text=values, textposition='auto', marker=dict(color=bar_colors)), row=row, col=item%3+1)
        fig.add_trace(go.Scatter(x=[-1, len(values)], y=[entropy_mean, entropy_mean],
                                 mode='lines', name='Mean', line=dict(color='red')), row=row, col=item%3+1)
        fig.add_trace(go.Scatter(x=[-1, len(values)], y=[entropy_mean + entropy_std, entropy_mean + entropy_std],
                                 mode='lines', name='Mean + Std', line=dict(color='blue')), row=row, col=item%3+1)
        fig.add_trace(go.Scatter(x=[-1, len(values)], y=[entropy_mean - entropy_std, entropy_mean - entropy_std],
                                 mode='lines', name='Mean - Std', line=dict(color='blue')), row=row, col=item%3+1)
        
        item += 1
        if item % 3 == 0:
            row += 1

    fig.update_layout(height=300*4, showlegend=False, title=f"Output size = {output_size}")
    fig.show()
