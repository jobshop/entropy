import random
from source.generators.jsp_generator import Operation, JSPGenerator
from typing import List
from collections import Counter


class RandomJSPDatasetGeneratorWithJobPoolEntropy(JSPGenerator):
    def __init__(self, dataset_size:int, num_jobs: int, num_operations: int, max_op_duration: int = 9, entropy_distribution: List = None):
        self.dataset_size = dataset_size
        self.num_jobs = num_jobs
        self.num_operations = num_operations
        self.max_op_duration = max_op_duration
        self.entropy_distribution = entropy_distribution
        self.total_number_of_jobs = dataset_size*num_jobs

    def generate(self):
        # Generating the job pool, it's the size of the job_entropy_distribution list
        jobs_pool = []

        for job in range(0, len(self.entropy_distribution)):
            operations = [] 
            for op in range(0, self.num_operations):
                duration = random.randint(1, self.max_op_duration)
                machine_type = random.randint(0, self.num_operations - 1)
                operations.append(Operation(machine_type, duration))
            jobs_pool.append(operations)

        # Creating job pool for the entire dataset
        dataset_job_pool = []
        for distr, job_index in zip(self.entropy_distribution, range(0, len(self.entropy_distribution))):
            dataset_job_pool += int(self.total_number_of_jobs*distr)*[job_index]


        # Following part is to fix the rounding issue of the multiplication distrubution*pool_size
        if len(dataset_job_pool) != self.total_number_of_jobs:
            
            size_difference = self.total_number_of_jobs - len(dataset_job_pool)

            freq_counts = Counter(dataset_job_pool)
            freq_dict = {k: v for k, v in freq_counts.items()}
            dataset_job_pool.sort(key=lambda x: freq_dict[x])

            dataset_job_pool += dataset_job_pool[:size_difference]

        random.shuffle(dataset_job_pool)


        # Splitting generated jobs into instances
        all_dataset = []

        for i in range(0, self.total_number_of_jobs, self.num_jobs):
            current_instance = dataset_job_pool[i:i+self.num_jobs]
            current_instance = [jobs_pool[job_index] for job_index in current_instance]
            all_dataset.append(current_instance)

        self.all_dataset = all_dataset
    

class RandomJSPDatasetGeneratorWithOperationPoolEntropy(JSPGenerator):
    def __init__(self, dataset_size:int, num_jobs: int, num_operations: int, max_op_duration: int = 9, entropy_distribution: List = None):
        self.dataset_size = dataset_size
        self.num_jobs = num_jobs
        self.num_operations = num_operations
        self.max_op_duration = max_op_duration
        self.entropy_distribution = entropy_distribution
        self.total_number_of_operations = dataset_size*num_jobs*num_operations

    def generate(self):
        print(f"available_ops={self.num_operations*self.max_op_duration} vs entropy_distr_len = {len(self.entropy_distribution)}")
        assert self.num_operations*self.max_op_duration >= len(self.entropy_distribution), "Not possible to generate unique operations list with given num_operations and max_op_duration"      

        # making sure that the random operations are unique
        random_operations = set()
        while len(random_operations) < len(self.entropy_distribution):
            random_operations.add((random.randint(1, self.max_op_duration), random.randint(0, self.num_operations - 1)))
        random_operations = list(random_operations)

        self.operations_pool = [Operation(i[1], i[0]) for i in random_operations]
        
        # Creating operation pool for the entire dataset
        dataset_operations_pool = []
        for distr, operation_index in zip(self.entropy_distribution, range(0, len(self.entropy_distribution))):
            dataset_operations_pool += int(self.total_number_of_operations*distr)*[operation_index]

        # Following part is to fix the rounding issue of the multiplication distrubution*pool_size
        if len(dataset_operations_pool) != self.total_number_of_operations:

            size_difference = self.total_number_of_operations - len(dataset_operations_pool)

            freq_counts = Counter(dataset_operations_pool)
            freq_dict = {k: v for k, v in freq_counts.items()}
            dataset_operations_pool.sort(key=lambda x: freq_dict[x])

            dataset_operations_pool += dataset_operations_pool[:size_difference]

        random.shuffle(dataset_operations_pool)

        self.dataset_operations_pool = dataset_operations_pool
        # Splitting generated jobs into instances
        all_dataset = []
        num_operations_per_instance = self.num_jobs*self.num_operations

        for i in range(0, self.total_number_of_operations, num_operations_per_instance):
            current_instance = dataset_operations_pool[i:i+num_operations_per_instance] # Selecting the operations indexes for the current instance
            current_instance = [self.operations_pool[job_index] for job_index in current_instance] # Selecting the operations from the operations pool
            current_instance = [current_instance[i:i + self.num_jobs] for i in range(0, len(current_instance), self.num_jobs)] # Splitting the operations into jobs
            all_dataset.append(current_instance)

        self.all_dataset = all_dataset
