import random
from source.generators.jsp_generator import Operation, JSPGenerator
from typing import List
from collections import Counter


class JSPInstanceGenerator(JSPGenerator):
    def generate(self):
        all_dataset = []
        for instance_id in range(0, self.dataset_size):
            all_dataset.append(self.generate_instance())
        self.all_dataset = all_dataset


class RandomJSPGenerator(JSPInstanceGenerator):
    """Fully random JSP generator."""
    def __init__(self, num_jobs: int, num_operations: int, max_op_duration: int = 9, dataset_size: int = 1000):
        self.num_jobs = num_jobs
        self.num_operations = num_operations
        self.max_op_duration = max_op_duration
        self.dataset_size = dataset_size

    def generate_instance(self):
        jobs = []
        
        for job_id in range(0, self.num_jobs):
            operations = []
            for op_id in range(0, self.num_operations):
                duration = random.randint(1, self.max_op_duration)
                type = random.randint(0, self.num_operations - 1)

                operations.append(Operation(type, duration))
            jobs.append(operations)

        return jobs 


class RandomJSPGeneratorOperationDistirbution(JSPInstanceGenerator):
    """JSP Generator that uses a pool of random operations to generate jobs.
    The pool is generated using a probability distribution list, to make sure that the job has a specific entropy."""
    def __init__(self, num_jobs: int, num_operations: int, max_op_duration: int = 9, dataset_size: int = 1000, entropy_distribution: List = None):
        self.num_jobs = num_jobs
        self.num_operations = num_operations
        self.max_op_duration = max_op_duration
        self.pool_size = self.num_jobs*self.num_operations
        self.dataset_size = dataset_size
        self.entropy_distribution = entropy_distribution
    
    def generate_instance(self):
        assert len(self.entropy_distribution) <= self.pool_size, "The size of the operation_distribution list does not match the pool_size."
        assert self.num_operations*self.max_op_duration >= len(self.entropy_distribution), "Not possible to generate unique operations list with given num_operations and max_op_duration"

        # Creating random operations in a way that ensures their uniqueness
        random_operations = set()
        while len(random_operations) < len(self.entropy_distribution):
            random_operations.add((random.randint(0, self.num_operations - 1), random.randint(1, self.max_op_duration)))
        random_operations = list(random_operations)


        operations_pool = []
        for distr, operation in zip(self.entropy_distribution, random_operations):
            operations_pool += int(self.pool_size*distr)*[operation]
        

        # Fixing the rounding issue of the multiplication distrubution*pool_size
        if len(operations_pool) != self.pool_size:
            size_difference = self.pool_size - len(operations_pool)

            freq_counts = Counter(operations_pool)
            freq_dict = {k: v for k, v in freq_counts.items()}
            operations_pool.sort(key=lambda x: freq_dict[x])

            operations_pool += operations_pool[:size_difference]
        
        random.shuffle(operations_pool)

        # Spliting operations into jobs
        jobs = []

        for job_id in range(0, self.num_jobs):
            job_operations = operations_pool[self.num_operations*job_id:self.num_operations*(job_id+1)]
            operations = [Operation(type, duration) for (type, duration) in job_operations]

            jobs.append(operations)

        return jobs

