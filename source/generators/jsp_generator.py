from typing import Dict
from collections import namedtuple
from pathlib import Path
from source.util.jsp_solver import JSPSolver
from source.conversion.writers import JSPWriterJSON
from source.util.entropy_functions import calculate_entropy_from_operations_list, collect_all_operations
import os


Operation = namedtuple("Operation", ["machine_type", "duration"])

class JSPGenerator:
    def __init__(self):
        pass

    def generate(self) -> Dict:
        raise NotImplementedError
    
    def save_dataset(self, dir_path, solve_optimal=True):

        dir_path = Path(dir_path)

        os.makedirs(dir_path, exist_ok=True)

        for i, instance_jobs in enumerate(self.all_dataset):

            if solve_optimal:
                optimal_time = JSPSolver().solve_from_job_list(instance_jobs)
            else:
                optimal_time = None

            all_operations = collect_all_operations(instance_jobs)
            
            instance = {"jobs": instance_jobs, 
                        "num_ops_per_job": self.num_operations, 
                        "max_op_time": self.max_op_duration,
                        "opt_time": optimal_time,
                        "intra-instance-operation-entropy": calculate_entropy_from_operations_list(all_operations, base=len(all_operations))
                        }
            
            #file_name = f"{dir_path.split('/')[-1]}-{i}.json"
            file_name = dir_path.name + "-" + str(i) + ".json"

            JSPWriterJSON().write_instance(instance, dir_path.joinpath(file_name), file_name.split(".")[0])
